package lab03;

public class Game {

	int _score=0;
	
	int rolls []= new int[26];
	// cases : 0=nothing; 1=spare; 2= strike;
	int bonuses [] =new int [12];
		
	int indexroll=0;
	int indexframe=0;
	
	public void roll(int pins){

		//is it a strike?
		if(isAStrike(pins)){
			rolls[indexframe*2]=pins;
			rolls[indexframe*2+1]=-1;
			bonuses[indexframe]=2;
			indexframe++;
		}
		else{
			rolls[indexframe*2+indexroll]=pins;
			if(indexroll==0)
				indexroll=1;
			else{
				
				//is it a spare?
				if(pins+rolls[indexframe*2]>=10){
					bonuses[indexframe]=1;
				}	
				indexframe++;
				indexroll=0;
			}
		}
	}
	
	
	private boolean isAStrike(int pins){
		return pins==10;
	}
	

	
	private void checkMaxScore(){
		if(_score>300)
			_score=300;
	}
	
	
	public int score(){
		System.out.println(indexframe);
		for(int i=0; i<10; i++){

			switch(bonuses[i]){
			//nothing to add
			case 0:
				_score+= rolls[i*2]+rolls[i*2+1];
				break;
			//there is a spare in this frame
			case 1: //the 2 rolls of the frame plus the next roll
				_score+= rolls[i*2]+rolls[i*2+1]+rolls[(i+1)*2];
				break;
			
			//there is a strike in this frame
			case 2:
				_score+= rolls[i*2]+rolls[(i+1)*2];
				if(rolls[(i+1)*2+1]==-1)
					_score+= rolls[(i+2)*2];
				else
					_score+=rolls[(i+1)*2+1];
				break;	
			}	
		}
		
		return _score;
	}
	
}
